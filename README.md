# How to isntall Ubuntu VM on VirtualBox


### Download Ubuntu desktop ISO image.

* [Download Ubuntu Desktop](https://www.ubuntu.com/download/desktop)



### Create a "new" vm

1. Select a name
2. Type: Linux
3. Version: Ubuntu (64-bit)

Memory: 1024 MB
Hard Disk: VDI, dynamically allocated, Maximum 20 GB


### Start the VM for the first time

1. Select the ISO file.
2. Install Ubuntu.




